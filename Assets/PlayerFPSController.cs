using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent (typeof (CharacterMovement))]
[RequireComponent (typeof (MouseLook))]
public class PlayerFPSController : MonoBehaviour
{

    private CharacterMovement characterMovement;
    private MouseLook mouseLook;

    private void Start()
    {
        //Hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Among Us").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();
    }

    private void Update()
    {
        movement ();
        rotation ();
    }

    /*
    public GameObject cam;
    public float walkSpeed = 5f;            // Walk Speed
    public float hRotationSpeed = 100f;     // Player rotates along Y axis
    public float vRotationSpeed = 80f;      // Cam rotates along X axis
    */

    private void movement()
    {
        // Movement
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown ("Jump");
        bool dashInput = Input.GetButton ("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);

        /*
        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));
        */

        //Rotation
        /*
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);
        cam.transform.Rotate(-vCamRotation, 0f, 0f);
        */
    }

    private void rotation ()
    {
        // Rotation
        float hRotationInput = Input.GetAxis ("Mouse X");
        float vRotationInput = Input.GetAxis ("Mouse Y");

        mouseLook.handleRotation (hRotationInput, vRotationInput);
    }
}