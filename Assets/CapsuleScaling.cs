using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    
    [SerializeField]
    private Vector3 axes;       // posibles ejes de escalado
    public float scaleUnits;    // velocidad de escalado

    // Update is called once per frame
    void Update()
    {
        
        //Acotacion de los valores de escalado al valor unitario [-1, 1]
        axes = CapsuleMovement.ClampVector3(axes);

        // La escala es acumulativa (contrario a la rotacion y el movimiento), esto quiere decir que
        // debemos anadir el nuevo valor de escala al al valor anterior
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}