using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public GameObject camerasParent;    // Parent object of all cameras that should rotate with the mouse
    public float hRotationSpeed = 100f; // Player rotates along Y axis
    public float vRotationSpeed = 80f;  // Cam rotates X axis
    public float maxVerticalAngle;      // Maximum rotation along X axis
    public float minVerticalAngle;      // Minimum rotation along X axis
    public float smoothTime = 0.05f;

    float vCamRotationAngles;           // Variable to apply Vertical Rotation
    float hPlayerRotation;              // Variable to apply Horizontal Rotation
    float currentHVelocity;             //Smooth Horizontal Velocity
    float currentVVelocity;             // Smooth Vertical Velocity
    float targetCamEulers;              // Variable to accumulate the Euler angles along X axis
    Vector3 targetCamRotation;          // Aux variable to store the target rotation of the camerasParent avoiding to instantiate a new Vector3 every frame
    
    void Start()
    {
        // Hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void handleRotation (float hInput, float vInput) {
        // Get rotation based on input
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        // Player Rotation
        hPlayerRotation = Mathf.SmoothDamp (hPlayerRotation, targetPlayerRotation, ref currentHVelocity, smoothTime);
        transform.Rotate (0f, hPlayerRotation, 0f);

        // CamRotation
        targetCamEulers = Mathf.Clamp (targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp (vCamRotationAngles, targetCamEulers, ref currentVVelocity, smoothTime);
        targetCamRotation.Set (-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */
}